package com.introspec.atmportal.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.introspec.atmportal.controller.AccountController;
import com.introspec.atmportal.model.dto.AccountDTO;
import com.introspec.atmportal.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class) //@WebMvcTest(value = StudentController.class, secure = false)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets", uriHost = "localhost", uriPort = 8999)
@EnableSpringDataWebSupport
@ActiveProfiles(value="test")
@WithMockUser(username = "user", password = "password")
public class AccountControllerTests {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AccountService accountService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAccountsTest() throws Exception {

        List<AccountDTO> accountDTOList = new ArrayList<>();
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccount_no("100001000");
        accountDTO.setAccount_name("Bola Eze Buhari");
        accountDTOList.add(accountDTO);
        AccountDTO accountDTO1 = new AccountDTO();
        accountDTO1.setAccount_no("300005000");
        accountDTO1.setAccount_name("Simeon Dan Shittu");
        accountDTOList.add(accountDTO1);

        Page<AccountDTO> page = new PageImpl<>(accountDTOList);
        when( accountService.findAll(any(Pageable.class))).thenReturn(page);

        this.mockMvc.perform(get("/accounts?size=2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload[0].account_no", Matchers.is("100001000")))
                .andExpect(jsonPath("$.payload[0].account_name", Matchers.is("Bola Eze Buhari")))
                .andExpect(jsonPath("$.payload[1].account_no", Matchers.is("300005000")))
                .andExpect(jsonPath("$.payload[1].account_name", Matchers.is("Simeon Dan Shittu")))
                .andDo(
                    document("{class-name}/{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
//                        responseHeaders(
//                                headerWithName("X-Users-Total").description("The total amount of users")
//                        ),
                        requestParameters(
                            parameterWithName("size").description("Limit the number of accounts obtained to this number.")
                        ),
                        responseFields(
                            fieldWithPath("payload[].account_no").description("Account number not less than 8 xters").type(JsonFieldType.STRING),
                            fieldWithPath("payload[].account_name").description("Account name not less than 3 xters").type(JsonFieldType.STRING),
                            fieldWithPath("meta.size").description("Size of the returned payload").type(JsonFieldType.NUMBER),
                            fieldWithPath("meta.number").description("Number of the returned payload. 0 means first page").type(JsonFieldType.NUMBER).optional(),
                            fieldWithPath("meta.number_of_elements").description("Number of elements of the returned payload").type(JsonFieldType.NUMBER).optional(),
                            fieldWithPath("meta.total_pages").description("Total pages of the returned payload").type(JsonFieldType.NUMBER).optional(),
                            fieldWithPath("meta.total_elements").description("Total elements of the returned payload").type(JsonFieldType.NUMBER).optional(),
                            fieldWithPath("meta.page_number").description("Page number of the returned payload").type(JsonFieldType.NUMBER).optional(),
                            fieldWithPath("meta.page_size").description("Page size of the returned payload").type(JsonFieldType.NUMBER).optional()
                        )
                    )
                );

    }

    @Test
    public void viewAccountTest() throws Exception {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccount_no("100001000");
        accountDTO.setAccount_name("Bola Eze Buhari");

        when(accountService.findOne(any(String.class))).thenReturn(accountDTO);

        this.mockMvc.perform(get("/accounts/100001000"))
                .andExpect(status().isOk())
                .andDo(
                        document("{class-name}/{method-name}",
                            preprocessRequest(prettyPrint()),
                            preprocessResponse(prettyPrint()),
                            responseFields(
                                fieldWithPath("account_no").description("Account number not less than 8 xters").type(JsonFieldType.STRING),
                                fieldWithPath("account_name").description("Account name not less than 3 xters").type(JsonFieldType.STRING)
                            )
                        )
                );

    }

    @Test
    public void createAccountTest() throws Exception {
        Map<String, String> crud = new HashMap<>();
        crud.put("account_name", "Tafa Doe");
        crud.put("account_no", "1000012222");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccount_no("1000012222");
        accountDTO.setAccount_name("Tafa Doe");

        when(accountService.save(any(AccountDTO.class))).thenReturn(accountDTO);

        ConstraintDescriptions constraintDescriptions = new ConstraintDescriptions(AccountDTO.class);

        this.mockMvc.perform(post("/accounts")
                .with(csrf())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(crud)))
                .andExpect(status().isCreated())
                .andDo(document("{class-name}/{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        requestFields(
                            fieldWithPath("account_name").description("System account name")
                                    .type(JsonFieldType.STRING)
                                    .optional()
                                    .attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("account_name"))),
                            fieldWithPath("account_no").description("System account number")
                                    .type(JsonFieldType.STRING)
                                    .attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("account_no")))
                        ),
                        responseFields(
                            fieldWithPath("account_no").description("System account number").type(JsonFieldType.STRING),
                            fieldWithPath("account_name").description("System account name").type(JsonFieldType.STRING)
                        )
                    )
                );
    }

    @Test
    public void deleteAccountTest() throws Exception {
        this.mockMvc.perform(delete("/accounts/{no}", "10").with(csrf()))
                .andExpect(status().isOk())
                .andDo(document("{class-name}/{method-name}",
                        pathParameters(
                            parameterWithName("no").description("The account number to delete")
                        )
                    )
                );

        verify(accountService).delete(anyString());

    }

    @Test
    public void updateAccountTest() throws Exception {
        Map<String, String> crud = new HashMap<>();
        crud.put("account_no", "1000012222");
        crud.put("account_name", "Tafa Doe New");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccount_no("1000012222");
        accountDTO.setAccount_name("Tafa Doe New");

        when(accountService.save(any(AccountDTO.class))).thenReturn(accountDTO);

        ConstraintDescriptions constraintDescriptions = new ConstraintDescriptions(AccountDTO.class);

        this.mockMvc.perform(put("/accounts", 10).contentType(MediaType.APPLICATION_JSON)
            .content(this.objectMapper.writeValueAsString(crud)).with(csrf()))
            .andExpect(status().isAccepted())
            .andDo(document("{class-name}/{method-name}",
                    preprocessRequest(prettyPrint()),
                    preprocessResponse(prettyPrint()),
                    requestFields(
                            fieldWithPath("account_name").description("Account name not less than 3 xters")
                                    .type(JsonFieldType.STRING)
                                    .optional()
                                    .attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("account_name"))),
                            fieldWithPath("account_no").description("Account number not less than 8 xters")
                                    .type(JsonFieldType.STRING)
                                    .attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("account_name")))
                    ),
                    responseFields(
                            fieldWithPath("account_no").description("Account number not less than 8 xters").type(JsonFieldType.STRING),
                            fieldWithPath("account_name").description("Account name not less than 3 xters").type(JsonFieldType.STRING)
                    )
                )
            );
    }

}
