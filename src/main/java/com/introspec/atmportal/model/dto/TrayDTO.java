package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.introspec.atmportal.model.enums.Currency;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrayDTO {

    private Long unload_denomination;
    private Long load_denomination;
    private Long load_count;
    private Long unload_count;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Currency load_currency;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Currency unload_currency;

}
