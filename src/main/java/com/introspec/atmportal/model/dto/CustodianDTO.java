package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustodianDTO {

    private Long id;

    @NotNull(message = ">-Username must not be null.")
    private String username;

    private Set<TerminalDTO> terminals = new HashSet<>();

}
