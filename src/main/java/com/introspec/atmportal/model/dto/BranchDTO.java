package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BranchDTO {

    @NotNull(message = ">-Branch name must not be null")
    private String code;

    @NotNull(message = ">-Branch name must not be null")
    private String name;

    @Size(min = 5, message = ">-Branch address too short")
    private String address;

    @Size(min = 8, message = ">-Account length too short (min=8xter)")
    private String gl_account;

    private String branch_head;

    private Set<BranchDTO> subs = new HashSet<>();

    private Set<TerminalDTO> terminals = new HashSet<>();
}
