package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InventoryDTO {

    private Long id;

    private String reference;

    private BigDecimal amount_unloaded;

    private BigDecimal amount_loaded;

    private LocalDateTime transaction_start_date;

    private LocalDateTime transaction_end_date;

    private Long cards_captured_count;

    private Long unload_reject_count;

    private Long load_reject_count;

    private TrayDTO[] trays;

    private String custodian;

    private String terminal;

}
