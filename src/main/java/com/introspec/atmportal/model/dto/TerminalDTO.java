package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.introspec.atmportal.model.enums.TerminalStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TerminalDTO {

    private String id;

    @Size(min = 9, message = "ip address too short")
    private String ip_address;

    @Enumerated(EnumType.STRING)
    private TerminalStatus status;

    @NotNull(message = ">-Terminal GL Account must not be null")
    @Size(min = 8, message = ">-GL Account too short (min=8xters)")
    private String gl_account;

    @NotNull(message = ">-Terminal GL Account must not be null")
    @Size(min = 3, message = ">-Terminal Branch name too short (min=3xters)")
    private BranchDTO branch;

}
