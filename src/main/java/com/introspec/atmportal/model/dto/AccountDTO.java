package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDTO {

    @NotNull(message = ">-Account number must not be null")
    private String account_no;

    @NotNull(message = ">-Account name must not be null")
    @Size(min = 3, message = ">-Account name too short. Should be at least 3 characters")
    private String account_name;

}
