package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.introspec.atmportal.model.Terminal;
import com.introspec.atmportal.model.enums.JournalType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JournalDTO {

    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private JournalType type;

    private Boolean error;

    private String message;

    private String terminal;

}
