package com.introspec.atmportal.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.introspec.atmportal.model.enums.TransactionCategory;
import com.introspec.atmportal.model.enums.TransactionType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionDTO {

    private Long id;

    private LocalDateTime ej_time;

    private String from_account;

    private String to_account; // this applies for transfers

    private String rrn;

    private String stan;

    private String pan;

    // *
    private String ledger, avail;

    private String sur_charge;

    @Enumerated(EnumType.STRING)
    private TransactionCategory transaction_category;

    @Enumerated(EnumType.STRING)
    private TransactionType transaction_type;

    private String error_message;

    private BigDecimal amount;

    private String cash_dispense_status;

    private String summary;

    private JournalDTO journal;

}
