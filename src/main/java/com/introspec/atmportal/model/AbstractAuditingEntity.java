package com.introspec.atmportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * Base abstract class for entities which will hold definitions for created By, created Date, last Modified By,
 * last modified by date.
 */
@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditingEntity<U> implements Serializable {

    protected static final long serialVersionUID = 1L;

    @CreatedBy
    @Column(name = "created_by", nullable = false, updatable = false)
    @JsonIgnore
    protected String createdBy;

    @CreatedDate
    @JsonIgnore
    @Column(name = "create_date", nullable = false, updatable = false)
    protected Instant createdDate = Instant.now();
//    protected LocalDateTime createdDate = LocalDateTime.now();

    @LastModifiedBy
    @JsonIgnore
    @Column(name = "last_modified_by")
    protected U lastModifiedBy;

    @LastModifiedDate
    @JsonIgnore
    @Column(name = "last_modified_date")
    protected Instant lastModifiedDate = Instant.now();

}

