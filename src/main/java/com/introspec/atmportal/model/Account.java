package com.introspec.atmportal.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "accounts")
public class Account extends AbstractAuditingEntity<String> {

    @Id
    @Column(name = "account_no")
    private String accNo;

    @NotNull(message = "Account name must not be null")
    private String name;

}
