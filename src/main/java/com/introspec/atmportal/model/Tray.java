package com.introspec.atmportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.introspec.atmportal.model.enums.Currency;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "trays")
@SequenceGenerator(name = "trayGenerator")
public class Tray extends AbstractAuditingEntity<String> {

    @Id
    private Long id;
    private Long unloadDenomination;
    private Long loadDenomination;
    private Long loadCount;
    private Long unloadCount;

    private Currency loadCurrency;

    private Currency unloadCurrency;

}
