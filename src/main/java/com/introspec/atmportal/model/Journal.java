package com.introspec.atmportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.introspec.atmportal.model.enums.JournalType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "journals")
@SequenceGenerator(name = "journalGenerator")
public class Journal extends AbstractAuditingEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "journalGenerator")
    private Long id;

    @NotNull(message = "-Journal name must not be null-")
    private String name;

    @Enumerated(EnumType.STRING)
    private JournalType type;

    private Boolean error;

    private String message;

    @ManyToOne
    @JoinColumn(name = "terminal_id")
    @JsonIgnore
//    @QueryInit("office")
    private Terminal terminal;

}
