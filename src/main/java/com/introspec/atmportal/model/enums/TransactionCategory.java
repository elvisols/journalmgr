package com.introspec.atmportal.model.enums;

public enum TransactionCategory {
    ON_US,
    NOT_ON_US;
}
