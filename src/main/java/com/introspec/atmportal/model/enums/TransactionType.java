package com.introspec.atmportal.model.enums;

public enum TransactionType {
    DEPOSIT,
    WITHDRAWAL,
    INQUIRY,
    TRANSFER,
    OTHERS;
}
