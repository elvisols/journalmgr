package com.introspec.atmportal.model.enums;

public enum JournalType {
    DAT,
    JRN,
    TXT,
    RTF,
    LOG
}
