package com.introspec.atmportal.model.enums;

public enum TerminalStatus {
    ACTIVE,
    INACTIVE;
}
