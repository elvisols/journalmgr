package com.introspec.atmportal.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "branches")
public class Branch extends AbstractAuditingEntity<String> {

    @Id
    private String code;

    @NotNull(message = "-Branch name must not be null-")
    private String name;

    @Size(min = 5, message = "-Branch address too short-")
    private String address;

    @ManyToOne
    @JoinColumn(name = "account_no")
    private Account glAccount;

    @OneToMany(mappedBy="head")
    private Set<Branch> subs = new HashSet<>();

    @OneToMany(mappedBy="branch")
    private Set<Terminal> terminals = new HashSet<>();

    @ManyToOne//(cascade={CascadeType.ALL})
    @JoinColumn(name="branch_head")
    private Branch head;

}
