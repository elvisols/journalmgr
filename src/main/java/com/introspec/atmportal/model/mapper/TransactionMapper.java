package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Transaction;
import com.introspec.atmportal.model.dto.TransactionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {

    @Mapping(source = "ejTime", target = "ej_time")
    @Mapping(source = "fromAccount", target = "from_account")
    @Mapping(source = "toAccount", target = "to_account")
    @Mapping(source = "surCharge", target = "sur_charge")
    @Mapping(source = "transactionCategory", target = "transaction_category")
    @Mapping(source = "transactionType", target = "transaction_type")
    @Mapping(source = "errorMessage", target = "error_message")
    @Mapping(source = "cashDispenseStatus", target = "cash_dispense_status")
    @Mapping(source = "journal.terminal.id", target = "journal.terminal")
    TransactionDTO toDto(Transaction transaction);

    @Mapping(target = "ejTime", source = "ej_time")
    @Mapping(target = "fromAccount", source = "from_account")
    @Mapping(target = "toAccount", source = "to_account")
    @Mapping(target = "surCharge", source = "sur_charge")
    @Mapping(target = "transactionCategory", source = "transaction_category")
    @Mapping(target = "transactionType", source = "transaction_type")
    @Mapping(target = "errorMessage", source = "error_message")
    @Mapping(target = "cashDispenseStatus", source = "cash_dispense_status")
    @Mapping(target = "journal.terminal.id", source = "journal.terminal")
    Transaction toEntity(TransactionDTO transactionDTO);

    default Transaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Transaction transaction = new Transaction();
        transaction.setId(id);
        return transaction;
    }

}
