package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Custodian;
import com.introspec.atmportal.model.Inventory;
import com.introspec.atmportal.model.Terminal;
import com.introspec.atmportal.model.dto.InventoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {Terminal.class, Custodian.class})
public interface InventoryMapper extends EntityMapper<InventoryDTO, Inventory> {

    @Mapping(source = "custodian.username", target = "custodian")
    @Mapping(source = "terminal.id", target = "terminal")
    @Mapping(source = "amountUnloaded", target = "amount_unloaded")
    @Mapping(source = "amountLoaded", target = "amount_loaded")
    @Mapping(source = "transactionStartDate", target = "transaction_start_date")
    @Mapping(source = "transactionEndDate", target = "transaction_end_date")
    @Mapping(source = "cardsCapturedCount", target = "cards_captured_count")
    @Mapping(source = "unloadRejectCount", target = "unload_reject_count")
    @Mapping(source = "loadRejectCount", target = "load_reject_count")
    InventoryDTO toDto(Inventory inventory);

    @Mapping(target = "custodian.username", source = "custodian")
    @Mapping(target = "terminal.id", source = "terminal")
    @Mapping(target = "amountUnloaded", source = "amount_unloaded")
    @Mapping(target = "amountLoaded", source = "amount_loaded")
    @Mapping(target = "transactionStartDate", source = "transaction_start_date")
    @Mapping(target = "transactionEndDate", source = "transaction_end_date")
    @Mapping(target = "cardsCapturedCount", source = "cards_captured_count")
    @Mapping(target = "unloadRejectCount", source = "unload_reject_count")
    @Mapping(target = "loadRejectCount", source = "load_reject_count")
    Inventory toEntity(InventoryDTO inventoryDTO);

    default Inventory fromId(Long id) {
        if (id == null) {
            return null;
        }
        Inventory inventory = new Inventory();
        inventory.setId(id);
        return inventory;
    }

}
