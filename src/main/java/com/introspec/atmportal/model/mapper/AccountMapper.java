package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Account;
import com.introspec.atmportal.model.dto.AccountDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface AccountMapper extends EntityMapper<AccountDTO, Account> {

    @Mapping(source = "accNo", target = "account_no")
    @Mapping(source = "name", target = "account_name")
    AccountDTO toDto(Account account);

    @Mapping(source = "account_no", target = "accNo")
    @Mapping(source = "account_name", target = "name")
    Account toEntity(AccountDTO accountDTO);

    default Account fromId(String accNo) {
        if (accNo == null) {
            return null;
        }
        Account account = new Account();
        account.setAccNo(accNo);
        return account;
    }

}
