package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Account;
import com.introspec.atmportal.model.Branch;
import com.introspec.atmportal.model.Terminal;
import com.introspec.atmportal.model.dto.TerminalDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface TerminalMapper extends EntityMapper<TerminalDTO, Terminal> {

    @Mapping(source = "glAccount.accNo", target = "gl_account")
//    @Mapping(source = "branch.code", target = "branch")
    TerminalDTO toDto(Terminal terminal);

    @Mapping(source = "gl_account", target = "glAccount.accNo")
//    @Mapping(source = "branch", target = "branch.code")
    Terminal toEntity(TerminalDTO terminalDTO);

    default Terminal fromId(String id) {
        if (id == null) {
            return null;
        }
        Terminal terminal = new Terminal();
        terminal.setId(id);
        return terminal;
    }

}
