package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Custodian;
import com.introspec.atmportal.model.dto.CustodianDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface CustodianMapper extends EntityMapper<CustodianDTO, Custodian> {

//    @Mapping(source = "journal.terminal.id", target = "journal.terminal")
    CustodianDTO toDto(Custodian custodian);

    Custodian toEntity(CustodianDTO custodianDTO);

    default Custodian fromId(Long id) {
        if (id == null) {
            return null;
        }
        Custodian custodian = new Custodian();
        custodian.setId(id);
        return custodian;
    }
    
}
