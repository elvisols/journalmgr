package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Branch;
import com.introspec.atmportal.model.dto.BranchDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface BranchMapper extends EntityMapper<BranchDTO, Branch> {

    @Mapping(source = "glAccount.accNo", target = "gl_account")
    @Mapping(source = "head.code", target = "branch_head")
    BranchDTO toDto(Branch branch);

    @Mapping(source = "gl_account", target = "glAccount.accNo")
    @Mapping(source = "branch_head", target = "head.code")
    Branch toEntity(BranchDTO branchDTO);

    default Branch fromId(String code) {
        if (code == null) {
            return null;
        }
        Branch branch = new Branch();
        branch.setCode(code);
        return branch;
    }

}
