package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Tray;
import com.introspec.atmportal.model.dto.TrayDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TrayMapper extends EntityMapper<TrayDTO, Tray> {

    @Mapping(source = "unloadDenomination", target = "unload_denomination")
    @Mapping(source = "loadDenomination", target = "load_denomination")
    @Mapping(source = "unloadCount", target = "unload_count")
    @Mapping(source = "loadCurrency", target = "load_currency")
    @Mapping(source = "unloadCurrency", target = "unload_currency")
    TrayDTO toDto(Tray tray);

    @Mapping(target = "unloadDenomination", source = "unload_denomination")
    @Mapping(target = "loadDenomination", source = "load_denomination")
    @Mapping(target = "unloadCount", source = "unload_count")
    @Mapping(target = "loadCurrency", source = "load_currency")
    @Mapping(target = "unloadCurrency", source = "unload_currency")
    Tray toEntity(TrayDTO trayDTO);

    default Tray fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tray tray = new Tray();
        tray.setId(id);
        return tray;
    }

}
