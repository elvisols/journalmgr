package com.introspec.atmportal.model.mapper;

import com.introspec.atmportal.model.Journal;
import com.introspec.atmportal.model.dto.JournalDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface JournalMapper extends EntityMapper<JournalDTO, Journal> {

    @Mapping(source = "terminal.id", target = "terminal")
    JournalDTO toDto(Journal journal);

    @Mapping(source = "terminal", target = "terminal.id")
    Journal toEntity(JournalDTO journalDTO);

    default Journal fromId(Long id) {
        if (id == null) {
            return null;
        }
        Journal journal = new Journal();
        journal.setId(id);
        return journal;
    }

}
