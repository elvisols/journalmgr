package com.introspec.atmportal.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.introspec.atmportal.model.enums.TransactionCategory;
import com.introspec.atmportal.model.enums.TransactionType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "transactions")
@SequenceGenerator(name = "transactionGenerator")
public class Transaction  extends AbstractAuditingEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transactionGenerator")
    private Long id;

    @Column(name = "ej_time")
    private LocalDateTime ejTime;

    @Column(name = "from_account")
    private String fromAccount;

    @Column(name = "to_account")
    private String toAccount; // this applies for transfers

    private String rrn;

    private String stan;

    private String pan;

    // *
    private String ledger, avail;

    private String surCharge;

    @Enumerated(EnumType.STRING)
    private TransactionCategory transactionCategory;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Column(columnDefinition = "TEXT")
    private String errorMessage;

    private BigDecimal amount;
    private String cashDispenseStatus;
    private String summary;

    @ManyToOne()
    @JoinColumn(name = "journal_id", referencedColumnName = "id")
    private Journal journal;

}
