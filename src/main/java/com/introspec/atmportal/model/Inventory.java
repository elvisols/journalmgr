package com.introspec.atmportal.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "inventories")
@SequenceGenerator(name = "inventoryGenerator")
public class Inventory extends AbstractAuditingEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inventoryGenerator")
    private Long id;

    private String reference;

    @Column(name = "amount_unloaded")
    private BigDecimal amountUnloaded;

    @Column(name = "amount_loaded")
    private BigDecimal amountLoaded;

    @Column(name = "transaction_start_date")
    private LocalDateTime transactionStartDate;

    @Column(name = "transaction_end_date")
    private LocalDateTime transactionEndDate;

    @Column(name = "cards_captured_count")
    private Long cardsCapturedCount;

    @Column(name = "unload_reject_count")
    private Long unloadRejectCount;

    @Column(name = "load_reject_count")
    private Long loadRejectCount;

    @Lob
    private Tray[] trays;

    @ManyToOne
    @JoinColumn(name = "custodian_id")
    private Custodian custodian;

    @ManyToOne()
    @JoinColumn(name = "terminal_id", referencedColumnName = "id")
    private Terminal terminal;

}
