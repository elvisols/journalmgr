package com.introspec.atmportal.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@SequenceGenerator(name = "custodianGenerator")
@Table(name = "custodians")
public class Custodian {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "custodianGenerator")
    private Long id;

    @NotNull(message = "Username must not be null.")
    private String username; // TODO: objectify the username and map it to a user class

    @ManyToMany(cascade = { CascadeType.ALL },fetch = FetchType.EAGER )
    @JoinTable(
            name = "custodian_terminals",
            joinColumns = @JoinColumn(name = "custodian_id"),
            inverseJoinColumns = @JoinColumn(name = "terminal_id"))
    private Set<Terminal> terminals = new HashSet<>();

}
