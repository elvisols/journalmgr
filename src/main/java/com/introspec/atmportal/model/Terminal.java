package com.introspec.atmportal.model;

import com.introspec.atmportal.model.enums.TerminalStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "terminals")
public class Terminal extends AbstractAuditingEntity<String> {

    @Id
    private String id;

    @Column(name = "ip_address")
    private String ipAddress;

    @Enumerated(EnumType.STRING)
    private TerminalStatus status;

    @ManyToOne
    @JoinColumn(name = "account_no")
    private Account glAccount;

    @ManyToOne
    @JoinColumn(name = "branch_code")
    private Branch branch;

}
