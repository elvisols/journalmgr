package com.introspec.atmportal.controller;

import com.introspec.atmportal.controller.util.ResponseWrapper;
import com.introspec.atmportal.controller.util.Siri;
import com.introspec.atmportal.model.dto.AccountDTO;
import com.introspec.atmportal.service.AccountService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.Positive;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.stream.Collectors;

/**
 * REST controller for managing Account.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class AccountController {

    private static final String ENTITY_NAME = "accounts";

    private final AccountService accountService;

//    public AccountController(AccountService accountService) {
//        this.accountService = accountService;
//    }

    /**
     * POST  /accounts : Create a new accounts.
     *
     * @param accountDTO the accountDTO to create
     * @return the ResponseEntity with status 400 (Bad Request) if the accounts has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/"+ENTITY_NAME)
    public ResponseEntity<AccountDTO> createAccounts(
//            @Valid @Positive(message = "Page number should be a positive number") @RequestParam(required = false, defaultValue = "1") int page,
//            @Valid @Positive(message = "Page size should be a positive number") @RequestParam(required = false, defaultValue = "10") int size,
            @Valid @RequestBody AccountDTO accountDTO, Errors errors) throws URISyntaxException, NotFoundException {
        log.debug("REST request to save {} : {}", ENTITY_NAME, accountDTO);

        Assert.notNull(accountDTO.getAccount_no(), "The account number must not be null!");

        Siri.checkForErrors(ENTITY_NAME, errors);

        AccountDTO result = accountService.save(accountDTO);

        return ResponseEntity.created(new URI(ENTITY_NAME + "/" + result.getAccount_no())).body(result);
    }

    /**
     * PUT  /accounts : Updates an existing accounts.
     *
     * @param accountDTO the accountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accountDTO,
     * or with status 400 (Bad Request) if the accountDTO is not valid,
     * or with status 500 (Internal Server Error) if the accountDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/"+ENTITY_NAME)
    public ResponseEntity<AccountDTO> updateAccounts(@Valid @RequestBody AccountDTO accountDTO, Errors errors) throws URISyntaxException, NotFoundException {
        log.debug("REST request to update {} : {}", ENTITY_NAME, accountDTO);

        Siri.checkForErrors(ENTITY_NAME, errors);

        AccountDTO result = accountService.save(accountDTO);

        return ResponseEntity.accepted().body(result);
    }

    /**
     * GET  /accounts : get all the accounts.
     *
     * @param pageable the pagination information
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of accounts in body
     */
    @GetMapping("/"+ENTITY_NAME)
    public ResponseEntity<ResponseWrapper> getAllAccounts(@RequestParam(value = "app", defaultValue="all") String app, Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Account for app: {}", app);
        Page<AccountDTO> page = accountService.findAll(pageable);
        log.debug("Page is {}" , page);
        return new ResponseEntity<>(new ResponseWrapper(page), HttpStatus.OK);
    }

    /**
     * GET  /accounts/:no : get the "no" accounts.
     *
     * @param no the id of the accountsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/"+ENTITY_NAME+"/{no}")
    public ResponseEntity<AccountDTO> getAccounts(@RequestParam(value = "app", defaultValue="all") String app, @PathVariable String no) {
        log.debug("REST request to get Account :{}, App:{}", no, app);

        AccountDTO accountsDTO = accountService.findOne(no);

        return new ResponseEntity<>(accountsDTO, HttpStatus.OK);

    }

    /**
     * DELETE  /accounts/:no : delete the "no" accounts.
     *
     * @param no the no of the accountsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/"+ENTITY_NAME+"/{no}")
    public ResponseEntity<Void> deleteAccounts(@PathVariable String no) {
        log.debug("REST request to delete account: {}", no);

        accountService.delete(no);

        return ResponseEntity.ok().build();
    }

}
