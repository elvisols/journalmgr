package com.introspec.atmportal.controller.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseWrapper {

	private Object payload;
	
	private Page<?> page;

	private MetaFields meta = new MetaFields();

	public ResponseWrapper(Page<?> page) {
		this.payload = page.getContent();
		this.meta.setSize(page.getSize());
		this.meta.setNumber(page.getNumber());
		this.meta.setNumber_of_elements(page.getNumberOfElements());
		this.meta.setTotal_pages(page.getTotalPages());
		this.meta.setTotal_elements(page.getTotalElements());
		this.page = null;
	}

	@Data
	class MetaFields {
		private int size;
		private int number;
		private int number_of_elements;
		private int total_pages;
		private long total_elements;
		private int page_number;
		private int page_size;
	}

}
