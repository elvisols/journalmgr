package com.introspec.atmportal.controller.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.Errors;

import javax.validation.ValidationException;
import java.util.stream.Collectors;

@Slf4j
public class Siri {

    public static void checkForErrors(String entity, Errors errors) {
        if (errors.hasErrors()) {
            log.error("Error in creating new {} detected below...\n{}", entity, errors.getAllErrors());
            throw new ValidationException(errors.getAllErrors().stream()
                    .map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(", ")));
        }
    }

}
