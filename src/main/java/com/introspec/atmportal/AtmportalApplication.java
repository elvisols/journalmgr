package com.introspec.atmportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class AtmportalApplication {

    public static void main(String[] args) {
        SpringApplication.run(AtmportalApplication.class, args);
    }

}
