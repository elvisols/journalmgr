package com.introspec.atmportal.repository;

import com.introspec.atmportal.model.Tray;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrayRepository extends JpaRepository<Tray, Long> {
}
