package com.introspec.atmportal.repository;

import com.introspec.atmportal.model.Custodian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustodianRepository extends JpaRepository<Custodian, Long> {
}
