package com.introspec.atmportal.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class RouteBuilders extends RouteBuilder {

    @Override
    public void configure() throws Exception {
//        from("file:/Users/user/Documents/Files/settlement/settlement-sample-files/dumpit")
//                .log("...file received!!!");

        /*
        from("smb://devop@192.168.1.50/Technology/dumpit/1?password=Active_secure1&download=true&localWorkDirectory=/tmp/${file:name}-${file:modified}&idempotentKey=${file:name}-${file:modified}") // delay=60000(download once every hour) ...localWorkDirectory=C:\dumpit\1
                .log("... downloading ...")
                .to("file:/Users/user/Documents/Files/settlement/settlement-sample-files/dumpit")
                .log(">>> completed <<<");
        */

//        from("smb://networkDriveOne?noop=true&fileName=inputFile.csv
//                &idempotentKey=${file:name}-${file:modified}")
//                .to("smb://networkDriveTwo?fileExist=Override")

//        docker run -v /opt/docker/volumes/mssql:/var/opt/mssql \
//        --name=mssql -I \
//        -e ACCEPT_EULA=Y -e SA_PASSWORD=Password23 \
//        -p 1433:1433 \
//        -d microsoft/mssql-server-linux:2017-latest

    }

}
