package com.introspec.atmportal.service;

import com.introspec.atmportal.model.dto.AccountDTO;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface AccountService {
    /**
     * Save account.
     *
     * @param accountDTO the entity to save
     * @return the persisted entity
     */
    AccountDTO save(AccountDTO accountDTO) throws NotFoundException;

    /**
     * Get all the accounts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AccountDTO> findAll(Pageable pageable);

    /**
     * Get the "id" accounts.
     *
     * @param accNo the id of the entity
     * @return the entity
     */
    AccountDTO findOne(String accNo);

    /**
     * Delete the "accountNo" accounts.
     *
     * @param accNo the accountNo of the entity
     */
    void delete(String accNo);
    
}
