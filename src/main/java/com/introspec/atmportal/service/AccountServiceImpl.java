package com.introspec.atmportal.service;

import com.introspec.atmportal.model.Account;
import com.introspec.atmportal.model.dto.AccountDTO;
import com.introspec.atmportal.model.mapper.AccountMapper;
import com.introspec.atmportal.repository.AccountRepository;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.util.Optional;

/**
 * Service Implementation for managing Account.
 */
@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {
    
    private final AccountRepository accountRepository;

    private final AccountMapper accountMapper;

//    public AccountServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper, PermissionMapper permissionMapper) {
//        this.accountRepository = accountRepository;
//        this.accountMapper = accountMapper;
//    }

    @Override
    public AccountDTO save(AccountDTO accountDTO) throws NotFoundException {
        log.debug("Request to save AccountDTO : {}", accountDTO);

        Account account = accountMapper.toEntity(accountDTO);

        log.debug("Account : {}", account);

        account = accountRepository.save(account);

        return accountMapper.toDto(account);

    }

    /**
     * Get all the accounts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AccountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Account");

        return accountRepository.findAll(pageable)
                .map(accountMapper::toDto);
    }

    /**
     * Get one accounts by id.
     *
     * @param accNo the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AccountDTO findOne(String accNo) {
        log.debug("Request to get Account : {}", accNo);

        Optional<AccountDTO> accountDTO = accountRepository.findById(accNo)
                .map(accountMapper::toDto);

        if (!accountDTO.isPresent()) {
            throw new ValidationException("No account was found for no: " + accNo);
        }

        return accountDTO.get();
    }

    /**
     * Delete the accounts by id.
     *
     * @param accNo the id of the entity
     */
    @Override
    public void delete(String accNo) {
        log.debug("Request to delete Account : {}", accNo);

        accountRepository.deleteById(accNo);
    }
    
}
