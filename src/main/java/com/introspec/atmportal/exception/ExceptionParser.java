package com.introspec.atmportal.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 */
@Slf4j
@ControllerAdvice
public class ExceptionParser {

	private Map<String, Object> errors = new HashMap<>();
	
	@ExceptionHandler(ValidationException.class)
	public @ResponseBody
    Object handleCustomException(ValidationException ve, HttpServletRequest request) {
		log.info("...caught validation exception...");

		errors.put("status", HttpStatus.BAD_REQUEST.value());
		errors.put("message", ve.getMessage().isEmpty() ? "Missing Object ID" : Arrays.asList(ve.getMessage().split("\\s*,\\s*")));

		return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
 
	}

	@ExceptionHandler(ServletRequestBindingException.class)
	public final ResponseEntity<Object> handleHeaderException(Exception ex, WebRequest request) {

		errors.put("status", HttpStatus.BAD_REQUEST.value());
		errors.put("message", ex.getLocalizedMessage());

		return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(Exception.class)
	public @ResponseBody
    Object handleGeneralException(Exception e, HttpServletRequest request) throws Exception {
		log.info("...caught generic exception...");

		errors.put("status", HttpStatus.BAD_REQUEST.value());
		errors.put("message", e.getMessage());

		e.printStackTrace();

		return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
}
